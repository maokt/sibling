# sibling

Shebang lines must have the full path to the interpreter. When that full path to the desired interpreter isn't known, it is common to use the "env" program to find and run it from PATH. "Sibling" is similar, but checks the script's directory first.